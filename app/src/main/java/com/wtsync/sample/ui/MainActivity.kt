package com.wtsync.sample.ui

import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.MediaSourceEventListener
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSource
import com.wtsync.sample.R
import com.wtsync.sample.utill.showError
import com.wtsync.sample.utill.showToast
import com.wtsync.sdk.SyncListener
import com.wtsync.sdk.SyncLiveEdgeListener
import com.wtsync.sdk.SyncSdk
import com.wtsync.sdk.data.SyncClient
import com.wtsync.sdk.data.SyncInfo
import java.net.CookieManager
import java.net.CookiePolicy
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity(), SyncListener, SyncLiveEdgeListener, Player.Listener, MediaSourceEventListener {

    // Const string keys to store values
    private val SYNC_SERVICE_KEY = "sync_service_key"
    // Bundle object to save and get values
    private val mBundle = Bundle()

    private var mSyncSdk: SyncSdk? = null
    private var mDisplayName = ""
    private var mAccessToken = "PUT_TOKEN"
    private var mCalculatedCurrentPosition = 0L
    private var mVideoUrl = ""
    private val dateFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
    private var mTimer: Timer? = null

    private val mClientsAdapter: ClientsAdapter = ClientsAdapter()

    private var mExoPlayerView: StyledPlayerView? = null
    private lateinit var mExoPlayer: ExoPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // To prevent screen to be switched off
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        // Extract session's params
        intent.extras.also { extras: Bundle? ->
            mDisplayName = extras?.get(StartActivity.GROUP_NAME_CODE).toString()
            mVideoUrl = extras?.get(StartActivity.VIDEO_URL_CODE).toString()
        }

        mExoPlayer = ExoPlayer.Builder(this).build()

        val recyclerViewers: RecyclerView = findViewById(R.id.recycler_viewers)
        recyclerViewers.adapter = mClientsAdapter
        val btnStartSync: AppCompatButton = findViewById(R.id.btn_start_sync)
        val btnStopSync: AppCompatButton = findViewById(R.id.btn_stop_sync)
        val btnGroupPlay: AppCompatButton = findViewById(R.id.btn_group_play)
        val btnGroupPause: AppCompatButton = findViewById(R.id.btn_group_pause)
        val btnGroupPrevious: AppCompatButton = findViewById(R.id.btn_group_seek_previous)
        val btnGroupNext: AppCompatButton = findViewById(R.id.btn_group_seek_next)

        btnStartSync.setOnClickListener {
            mSyncSdk?.startSync()
            btnStartSync.isEnabled = false
            btnStopSync.isEnabled = true
            btnGroupPlay.isEnabled = true
        }
        btnStopSync.setOnClickListener {
            mSyncSdk?.stopSync()
            btnStartSync.isEnabled = true
            btnStopSync.isEnabled = false
            mClientsAdapter.clearClients()
        }
        btnGroupPlay.setOnClickListener {
            mExoPlayer.play()
            mSyncSdk?.groupPlay()
        }
        btnGroupPause.setOnClickListener {
            mExoPlayer.pause()
            mSyncSdk?.groupPause()
        }

        btnGroupPrevious.setOnClickListener {
            seekToPosition(false, getPlayerCalculatedPosition())
        }
        btnGroupNext.setOnClickListener {
            seekToPosition(true, getPlayerCalculatedPosition())
        }

        initPlayer()
        initSyncSdk()
    }

    private fun initSyncSdk() {
        mSyncSdk = SyncSdk.SyncSdkBuilder()
            .accessToken(mAccessToken)
            .name(mDisplayName)
            .syncListener(this)
            .syncLiveEdgeListener(this)
            .build(this)
    }

    private fun initPlayer() {
        val defaultCookieManager = CookieManager()
        defaultCookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER)

        mExoPlayerView = findViewById(R.id.player)
        mExoPlayerView?.requestFocus()

        mExoPlayer.addListener(this)
        mExoPlayer.playWhenReady = true
        mExoPlayerView?.player = mExoPlayer

        onPlay(mVideoUrl)
    }

    private fun seekToPosition(isForward: Boolean, position: Long) {
        if (mExoPlayer.isPlaying && mExoPlayer.playbackState == Player.STATE_READY && !mExoPlayer.isCurrentMediaItemLive) {
            val sign = if (isForward) 1 else -1
            val seekTo = position + (300_000L * sign)  // seek to 5 minutes forward or back
            if (seekTo >= 0) {
                mSyncSdk?.groupSeek(seekTo)
                mExoPlayer.seekTo(seekTo)
                Toast.makeText(
                    applicationContext,
                    "onSeekGroupEvent sent position=${dateFormat.format(Date(seekTo))}",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun onPlay(url: String) {
        mTimer?.cancel()
        mTimer = null
        mTimer = Timer(System.currentTimeMillis().toString())
        mTimer?.schedule(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    if (mExoPlayer.isPlaying) {
                        findViewById<AppCompatTextView>(R.id.player_position).text =
                            dateFormat.format(Date(onGetPlayerPosition()))
                    }
                }
            }
        }, 0, 300)
        try {
            mExoPlayer.setMediaSource(
                buildMediaSource(
                    Uri.parse(url),
                    Handler(Looper.getMainLooper())
                )
            )
            mExoPlayer.prepare()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun buildMediaSource(
        uri: Uri,
        handler: Handler
    ): MediaSource {
        val mediaSource = HlsMediaSource.Factory(DefaultDataSource.Factory(this))
                .setAllowChunklessPreparation(true)
                .createMediaSource(MediaItem.fromUri(uri))
        mediaSource.addEventListener(handler, this)

        return mediaSource
    }

    override fun onPause() {
        super.onPause()
        mSyncSdk?.stopSync()
        mExoPlayer.pause()
        (findViewById<AppCompatButton>(R.id.btn_start_sync)).isEnabled = true
    }

    override fun onResume() {
        super.onResume()
        // Check if Sync service was stopped because of app was putted into background
        if (mBundle.getBoolean(SYNC_SERVICE_KEY, false)) {
            // Clear Sync service value in the default state
            mBundle.putBoolean(SYNC_SERVICE_KEY, false)
            // Start Sync service
            mSyncSdk?.startSync()
            (findViewById<AppCompatButton>(R.id.btn_start_sync)).isEnabled = false
            // Resume ExoPlayer
            mExoPlayer.play()
        }
    }

    override fun onBackPressed() {
        // If its onBackPressed, we do not suppose resuming Sync service automatically. so clear Sync service value in the default state
        mBundle.putBoolean(SYNC_SERVICE_KEY, false)
        super.onBackPressed()
    }

    override fun onDestroy() {
        super.onDestroy()
        mSyncSdk?.stopSync()
        mTimer?.cancel()
        mTimer = null
        mExoPlayer.stop()
        mExoPlayer.release()
        if (mExoPlayerView != null) {
            mExoPlayerView = null
        }
    }

    override fun onPlaybackStateChanged(state: Int) {
        val progress: ProgressBar = findViewById(R.id.contentLoadingProgressBar)
        when (state) {
            Player.STATE_BUFFERING -> progress.visibility = View.VISIBLE
            Player.STATE_READY -> progress.visibility = View.GONE
            Player.STATE_ENDED -> progress.visibility = View.GONE
            Player.STATE_IDLE -> progress.visibility = View.GONE
        }
    }

    override fun onPlayerError(error: PlaybackException) {
        if (!isFinishing) {
            val errorMessage = "Error while playing stream"
            showError(this@MainActivity, errorMessage)
            mExoPlayer.stop()
            onPlay(mVideoUrl)
        }
    }

    override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters) {
        findViewById<AppCompatTextView>(R.id.rate).text = playbackParameters.speed.toString()
    }

    override fun onClientList(syncClientList: List<SyncClient>) {
        mClientsAdapter.clearClients()
        mClientsAdapter.addClientsList(syncClientList)
    }

    override fun onPlaybackFromPosition(position: Long) {
        if (mExoPlayer.isPlaying && mExoPlayer.playbackState == Player.STATE_READY) {
            mExoPlayer.seekTo(position)
        }
    }

    override fun onSyncInfo(syncInfo: SyncInfo) {
        findViewById<AppCompatTextView>(R.id.delta).text =
            if (syncInfo.delta != 0L) "${syncInfo.delta} ms" else getString(R.string.txt_empty_string)
        findViewById<AppCompatTextView>(R.id.accuracy).text =
            if (syncInfo.accuracy != 0.0f) "${syncInfo.accuracy} %" else getString(R.string.txt_empty_string)
    }

    override fun onSyncDisconnected() {
        mClientsAdapter.clearClients()
        (findViewById<AppCompatButton>(R.id.btn_start_sync)).isEnabled = true
        (findViewById<AppCompatButton>(R.id.btn_stop_sync)).isEnabled = false
        showError(this, "Sync service was disconnected!")
    }

    override fun onSetPlaybackRate(rate: Float) {
        mExoPlayer.playbackParameters = PlaybackParameters(rate)
    }

    override fun onGetPlaybackRate(): Float {
        if (mExoPlayer.isPlaying && mExoPlayer.playbackState == Player.STATE_READY) {
            return mExoPlayer.playbackParameters.speed
        }
        return 0f
    }

    override fun onGetPlayerPosition(): Long {
        if (mExoPlayer.isPlaying && mExoPlayer.playbackState == Player.STATE_READY) {
            mCalculatedCurrentPosition = getPlayerCalculatedPosition()
            return mCalculatedCurrentPosition
        }
        return 0L
    }

    override fun onGetLiveEdgePosition(): Long? {
        if (mExoPlayer.isPlaying && mExoPlayer.playbackState == Player.STATE_READY && !mExoPlayer.isCurrentMediaItemLive) {
            // Be sure to return stream live edge position
            return mExoPlayer.duration
        }
        return null
    }

    override fun onSyncGroupPlay(clientId: String) {
        mExoPlayer.play()
        showToast(applicationContext, "From ${mClientsAdapter.getClientName(clientId)} received onSyncGroupPlay")
    }

    override fun onSyncGroupSeek(clientId: String?, position: Long) {
        if (!mExoPlayer.isCurrentMediaItemLive) {
            // if its VOD then player can seek to required position
            mExoPlayer.seekTo(position)
        }
        // Client should send success seek request every time onSyncGroupSeek called
        mSyncSdk?.groupSeekSuccess(position)
        if (clientId == null) {
            showToast(applicationContext,"From Server received onSyncGroupSeek position=$position")
        } else {
            showToast(applicationContext,"From ${mClientsAdapter.getClientName(clientId)} received onSyncGroupSeek position=$position")
        }
    }

    override fun onSyncGroupPause(clientId: String) {
        mExoPlayer.pause()
        showToast(applicationContext, "From ${mClientsAdapter.getClientName(clientId)} received onSyncGroupPause)")
    }

    private fun getPlayerCalculatedPosition(): Long {
        val position = mExoPlayer.currentPosition
        val streamPosition = getPlayerPositionFromTimeLine(mExoPlayer.currentTimeline)
        mCalculatedCurrentPosition = position + streamPosition
        return mCalculatedCurrentPosition
    }

    private fun getPlayerPositionFromTimeLine(timeline: Timeline?): Long {
        var windowStartTimeMs = 0L
        if (timeline == null || timeline.isEmpty) {
            return windowStartTimeMs
        }
        val window = timeline.getWindow(0, Timeline.Window())
        if (window.windowStartTimeMs > windowStartTimeMs) {
            windowStartTimeMs = window.windowStartTimeMs
        }
        return windowStartTimeMs
    }
}