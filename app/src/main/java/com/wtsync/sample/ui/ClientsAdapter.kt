package com.wtsync.sample.ui

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.wtsync.sample.R
import com.wtsync.sdk.data.SyncClient

class ClientsAdapter : RecyclerView.Adapter<ClientsAdapter.ClientViewHolder>() {

    private val mClients = ArrayList<SyncClient>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClientViewHolder {
        return ClientViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_client,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ClientViewHolder, position: Int) {
        val client = mClients[position]
        holder.name.text = client.name
        holder.local.text = holder.itemView.context.getString(if (client.isLocal) R.string.txt_local else R.string.txt_remote)
        holder.leader.text = holder.itemView.context.getString(if (client.isLeader) R.string.txt_leader else R.string.txt_empty_string)
    }

    override fun getItemCount(): Int {
        return mClients.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addClientsList(viewerList: List<SyncClient>) {
        mClients.addAll(viewerList)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun clearClients() {
        mClients.clear()
        notifyDataSetChanged()
    }

    fun getClientName(id: String): String {
        mClients.forEach { client ->
            if (id == client.id) {
                return client.name
            }
        }
        return ""
    }

    class ClientViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val name: AppCompatTextView = v.findViewById(R.id.name)
        val local: AppCompatTextView = v.findViewById(R.id.local)
        val leader: AppCompatTextView = v.findViewById(R.id.leader)
    }
}