package com.wtsync.sample.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import com.wtsync.sample.R


class StartActivity : AppCompatActivity() {

    companion object {
        const val GROUP_NAME_CODE = "group_name_code"
        const val VIDEO_URL_CODE = "video_url_code"
    }

    private lateinit var mDisplayName: AppCompatEditText
    private lateinit var mStreamUrl: AppCompatEditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        (findViewById<View>(R.id.app_version) as AppCompatTextView).text =
            applicationContext.packageManager.getPackageInfo(
                applicationContext.packageName,
                0
            ).versionName


        val sharedPref =
            getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)

        mDisplayName = findViewById(R.id.display_name)
        mDisplayName.setText(sharedPref.getString(GROUP_NAME_CODE, ""))
        mStreamUrl = findViewById(R.id.stream_url)
        if (TextUtils.isEmpty(sharedPref.getString(VIDEO_URL_CODE, ""))) {
            mStreamUrl.setText(R.string.str_stream_url)
        } else {
            mStreamUrl.setText(sharedPref.getString(VIDEO_URL_CODE, ""))
        }
        // Click Listener for sign in button
        (findViewById<Button>(R.id.sign_in)).setOnClickListener {
            signIn()
        }
        // Done actions sign in button click
        mDisplayName.setOnEditorActionListener { v, actionId, event ->
            when (actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    signIn()
                    true
                }
                else -> false
            }
        }
    }

    private fun signIn() {
        val displayName = mDisplayName.text.toString()
        val videoUrl = mStreamUrl.text.toString()
        val sharedPref =
            getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
                ?: return

        with(sharedPref.edit()) {
            putString(GROUP_NAME_CODE, displayName)
            apply()
        }
        startMainActivity(displayName, videoUrl)
    }

    private fun startMainActivity(
        displayName: String,
        videoUrl: String
    ) {
        val intent =
            Intent(this@StartActivity, MainActivity::class.java)
        intent.putExtra(GROUP_NAME_CODE, displayName)
        intent.putExtra(VIDEO_URL_CODE, videoUrl)
        // Start MainActivity with params
        startActivity(intent)
    }
}