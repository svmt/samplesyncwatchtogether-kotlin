package com.wtsync.sample.utill

import android.content.Context
import android.widget.Toast

fun showError(context: Context, errorMessage: String) {
    showToast(context, errorMessage)
}

fun showToast(context: Context, errorMessage: String) {
    val t = Toast.makeText(context, errorMessage, Toast.LENGTH_LONG)
    t.show()
}